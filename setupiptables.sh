#!/bin/sh

 IPT=/sbin/iptables
 LOCAL_IFACE=eth0
 INET_IFACE=usb0
 INET_ADDRESS=IP_KEY
# Flush the tables
 iptables -F INPUT
 iptables -F OUTPUT
 iptables -F FORWARD
 iptables -t nat -P PREROUTING ACCEPT
 iptables -t nat -P POSTROUTING ACCEPT
 iptables -t nat -P OUTPUT ACCEPT
# Allow forwarding packets:
 iptables -A FORWARD -p ALL -i $LOCAL_IFACE -o $INET_IFACE -j ACCEPT
 iptables -A FORWARD -i $INET_IFACE -o $LOCAL_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT
# Packet masquerading
 iptables -t nat -A POSTROUTING -o $INET_IFACE -j SNAT --to-source $INET_ADDRESS