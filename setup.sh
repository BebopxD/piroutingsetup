#!/usr/bin/env sh

HOTSPOT_IP_SUB="192.168.42"
HOTSPOT_IP="192.168.42.164"

FORWARD_COMMENT="#net.ipv4.ip_forward=1"
FORWARD="net.ipv4.ip_forward=1"
IP_KEY="IP_KEY"

echo "Preparing files"
sed -i s/${IP_KEY}/${HOTSPOT_IP_SUB}/ ./setuproutes
sed -i s/${IP_KEY}/${HOTSPOT_IP_SUB}/ ./dhcpd.conf
sed -i s/${IP_KEY}/${HOTSPOT_IP}/ ./setupiptables.sh

echo "Activating ipv4 forwarding in /etc/sysctl.conf"
sed -i s/${FORWARD_COMMENT}/${FORWARD}/ /etc/sysctl.conf
sysctl -system

## Reconfigure the eth0 interface
echo "Reconfiguring eth0 interface"
cp /etc/network/interfaces /etc/network/interfaces.bak
OLD_STRING="iface eth0 inet manual"
NEW_STRING="allow-hotplug eth0\niface eth0 inet static\naddress 10.10.0.1\nnetwork 10.10.0.0\nnetmask 255.255.255.0\nbroadcast 10.10.0.255\ngateway 10.10.0.1\n"
sed -i "/${OLD_STRING}/c \\${NEW_STRING}" /etc/network/interfaces

echo "Creating init script for setting up the routes on system startup"
cp ./setuproutes /etc/init.d/
chmod 755 /etc/init.d/setuproutes
update-rc.d setuproutes defaults

echo "Installing dhcp server and configuring it"
apt-get install isc-dhcp-server
mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bak
cp ./dhcpd.conf /etc/dhcp/

echo "\n====== Please restart your system now. Then you can execute setupiptables.sh to use the usb hotspot =========="
