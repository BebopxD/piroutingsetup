#Routing setup for Raspberry Pi

Basic setup
-----------

1. After checking out this repository on your Raspberry Pi check which ip you would get from your
phone usb hotspot by executing `ifconfig` and looking at the `inet` ipv4 address under the `usb0`
interface.

1. Next check the ipaddress in the `setup.sh` file in this repository and see if they match. On most
Android phones the preconfigured ip should do just fine. If they are not equal change them BEFORE 
you execute the script.

~~~~
HOTSPOT_IP_SUB="192.168.42"
HOTSPOT_IP="192.168.42.164"
~~~~
   
3. After configuring the ip and making sure, that the Raspberry Pi is connected to the internet,
you can actually execute the setup simply by calling `sudo ./setup.sh` in the repository directory.
It has to be executed as root, because it reaches system config files that are secured by filesystems 
permission and because it downloads a dhcp server. When the setup is done, reboot the Raspberry Pi.

1. When the system is started up again, the Raspberry Pi creates a subnet on its ethernet port with
range from **10.10.0.10** to **10.10.0.50** and is able to give new devices an new ip address out
of that range. It also allows static IPs that mach the pattern.

1. There is only one step missing from perfection, because all devices in the subnet don't have a
working internet connection. To provide that, you only have to execute `sudo ./setupiptables.sh` in
the repository's directory. In case of trouble the `resetiptables.sh` script would reset all settings
to default.